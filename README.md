# Advanced Git Workshop
Lab 12: Creating your first git hook

---

# Tasks

 - Creating your first hook
 
 - Testing your hook

---

## Preparations

 - Let's clone a repository to work with:
 
```
$ git clone https://oauth2:gkLjhh5W4te-cJngRBxB@gitlab.com/sela-git-advanced-workshop/static-web-app.git lab12
$ cd lab12
```

---

## Creating your first hook

 - Let's create a hook to avoid working directly on master (locally)
  
 - Create the hook file in **.git/hooks/pre-commit** with the content below:
```
#!/bin/sh

# Check to see if we are on master branch. Stop accidental commits
if [ $(git symbolic-ref HEAD 2>/dev/null) == "refs/heads/master" ]
then
  echo "Cannot commit directly to master branch"
  exit 1
fi
exit 0
```

---

## Testing your hook

 - To test your hook just perform any change and try to commit in master:
```
$ echo content > new-file.txt
$ git add new-file.txt
$ git commit -m "testing hook"
```
  
 - You will receive the error ***Cannot commit directly to master branch***
 

## Deleting your hook

 - To disbale the hook just delete or rename the hook file:
```
$ mv .git/hooks/pre-commit .git/hooks/pre-commit.disabled
```

 - Then you should be able to commit the changes:
```
$ git commit -m "testing disabled hook"
```